# translation of kbattleship.po to Finnish
# translation of kbattleship.po to
# KDE Finnish translation sprint participants:
# Kim Enkovaara <kim.enkovaara@iki.fi>, 2002, 2003, 2004.
# Teemu Rytilahti <teemu.rytilahti@d5k.net>, 2003.
# Ilpo Kantonen <ilpo@iki.fi>, 2005.
# Mikko Piippo <piippo@cc.helsinki.fi>, 2008.
# Teemu Rytilahti <teemu.rytilahti@kde-fi.org>, 2008.
# Tommi Nieminen <translator@legisign.org>, 2010, 2011, 2018, 2022.
# Lasse Liehu <lasse.liehu@gmail.com>, 2010, 2012, 2013, 2014.
msgid ""
msgstr ""
"Project-Id-Version: kbattleship\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-10-21 00:44+0000\n"
"PO-Revision-Date: 2022-08-02 22:52+0300\n"
"Last-Translator: Tommi Nieminen <translator@legisign.org>\n"
"Language-Team: Finnish <kde-i18n-doc@kde.org>\n"
"Language: fi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 20.12.0\n"
"X-POT-Import-Date: 2012-12-01 22:23:59+0000\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Kim Enkovaara, Ilpo Kantonen, Mikko Piippo, Tommi Nieminen"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr ""
"kim.enkovaara@iki.fi, ilpo@iki.fi, mikko.piippo@helsinki.fi, "
"translator@legisign.org"

#: controller.cpp:50
#, kde-format
msgid "Computer"
msgstr "Tietokone"

#. i18n: ectx: label, entry (Hostname), group (network)
#: knavalbattle.kcfg:9
#, kde-format
msgid "Default hostname for multiplayer games."
msgstr "Monen pelaajan pelien oletuskonenimi."

#. i18n: ectx: label, entry (Port), group (network)
#: knavalbattle.kcfg:13
#, kde-format
msgid "Default port for multiplayer games."
msgstr "Monen pelaajan pelien oletusportti."

#. i18n: ectx: label, entry (Nickname), group (general)
#: knavalbattle.kcfg:19
#, kde-format
msgid "User nickname."
msgstr "Käyttäjän kutsumanimi."

#. i18n: ectx: label, entry (EnableSounds), group (general)
#: knavalbattle.kcfg:22
#, kde-format
msgid "Whether sound effects should be played."
msgstr "Käytetäänkö äänitehosteita."

#. i18n: ectx: label, entry (AdjacentShips), group (general)
#: knavalbattle.kcfg:26
#, kde-format
msgid "Allow the ships to be adjacent without one empty space between them."
msgstr "Sallii vierekkäiset laivat, joiden välissä ei ole yhtään tyhjää tilaa."

#. i18n: ectx: label, entry (SeveralShips), group (general)
#: knavalbattle.kcfg:30
#, kde-format
msgid ""
"Allow multiple ships of any size, default: 4 of 1, 3 of 2, 2 of 3, 1 of 4."
msgstr ""
"Salli useita samankokoisia laivoja. Oletusarvoisesti: 4 kpl kokoa 1, 3 kpl "
"kokoa 2, 2 kpl kokoa 3 ja 1 kpl kokoa 4."

#. i18n: ectx: ToolBar (gameToolbar)
#: knavalbattleui.rc:29
#, kde-format
msgid "Game Toolbar"
msgstr "Pelityökalurivi"

# pmap: =/gen=Meritaistelun/
# pmap: =/elat=Meritaistelusta/
#: main.cpp:42
#, kde-format
msgid "Naval Battle"
msgstr "Meritaistelu"

#: main.cpp:44
#, kde-format
msgid "The KDE ship sinking game"
msgstr "KDE:n laivanupotuspeli"

#: main.cpp:45
#, kde-format
msgid ""
"(c) 2000-2005  Nikolas Zimmermann, Daniel Molkentin\n"
"(c) 2007 Paolo Capriotti"
msgstr ""
"© 2000–2005 Nikolas Zimmermann, Daniel Molkentin\n"
"© 2007 Paolo Capriotti"

#: main.cpp:50
#, kde-format
msgid "Paolo Capriotti"
msgstr "Paolo Capriotti"

#: main.cpp:50
#, kde-format
msgid "Current maintainer"
msgstr "Nykyinen ylläpitäjä"

#: main.cpp:51
#, kde-format
msgid "Nikolas Zimmermann"
msgstr "Nikolas Zimmermann"

#: main.cpp:51
#, kde-format
msgid "Project Founder, GUI Handling, Client/Server"
msgstr "Projektin perustaja, käyttöliittymä, Asiakas/Palvelin"

#: main.cpp:52
#, kde-format
msgid "Daniel Molkentin"
msgstr "Daniel Molkentin"

#: main.cpp:52
#, kde-format
msgid "Dialog Stuff, Client/Server"
msgstr "Ikkunat, Asiakas/Palvelin"

#: main.cpp:53
#, kde-format
msgid "Kevin Krammer"
msgstr "Kevin Krammer"

#: main.cpp:53
#, kde-format
msgid "Computer Player"
msgstr "Tietokonepelaaja"

#: main.cpp:55
#, kde-format
msgid "Johann Ollivier Lapeyre"
msgstr "Johann Ollivier Lapeyre"

#: main.cpp:55
#, kde-format
msgid "Game artwork"
msgstr "Grafiikka"

#: main.cpp:56
#, kde-format
msgid "Eugene Trounev"
msgstr "Eugene Trounev"

#: main.cpp:56
#, kde-format
msgid "Background"
msgstr "Tausta"

#: main.cpp:57
#, kde-format
msgid "Robert Wadley"
msgstr "Robert Wadley"

#: main.cpp:57
#, kde-format
msgid "Artwork for some of the ships"
msgstr "Osa laivoista"

#: main.cpp:58
#, kde-format
msgid "Riccardo Iaconelli"
msgstr "Riccardo Iaconelli"

#: main.cpp:58
#, kde-format
msgid "Welcome screen"
msgstr "Tervetuloruutu"

#: main.cpp:59
#, kde-format
msgid "Benjamin Adler"
msgstr "Benjamin Adler"

#: main.cpp:59
#, kde-format
msgid "Icon"
msgstr "Kuvake"

#: main.cpp:60
#, kde-format
msgid "Nils Trzebin"
msgstr "Nils Trzebin"

#: main.cpp:60
#, kde-format
msgid "Sounds"
msgstr "Äänet"

#: main.cpp:61
#, kde-format
msgid "Elmar Hoefner"
msgstr "Elmar Hoefner"

#: main.cpp:61
#, kde-format
msgid "GFX"
msgstr "GFX"

#: main.cpp:62
#, kde-format
msgid "Lukas Tinkl"
msgstr "Lukas Tinkl"

#: main.cpp:62
#, kde-format
msgid "Non-Latin1 Support"
msgstr "Tuki muille kuin latin1-merkistölle"

#: main.cpp:63
#, kde-format
msgid "Malte Starostik"
msgstr "Malte Starostik"

#: main.cpp:63
#, kde-format
msgid "Various improvements"
msgstr "Monia parannuksia"

#: main.cpp:64
#, kde-format
msgid "Albert Astals Cid"
msgstr "Albert Astals Cid"

#: main.cpp:64 main.cpp:65
#, kde-format
msgid "Various improvements and bugfixes"
msgstr "Monia parannuksia ja virheiden korjauksia"

#: main.cpp:65
#, kde-format
msgid "John Tapsell"
msgstr "John Tapsell"

#: main.cpp:66
#, kde-format
msgid "Inge Wallin"
msgstr "Inge Wallin"

#: main.cpp:66
#, kde-format
msgid "Bugfixes and refactoring"
msgstr "Virheenkorjaukset ja uudelleenjärjestely"

#: main.cpp:67
#, kde-format
msgid "Jakub Stachowski"
msgstr "Jakub Stachowski"

#: main.cpp:67
#, kde-format
msgid "DNS-SD discovery"
msgstr "DNS-SD havainto"

#: main.cpp:68
#, kde-format
msgid "Roney Gomes"
msgstr "Roney Gomes"

#: main.cpp:68
#, kde-format
msgid "Porting to KGameRenderer and QGraphicsView"
msgstr "KGameRendereriin ja QGraphicsView’hyn siirto"

#: main.cpp:75
#, kde-format
msgid "[URL]"
msgstr "[Osoite]"

#: main.cpp:75
#, kde-format
msgid "URL of a Naval Battle game server to connect to after startup"
msgstr ""
"Meritaistelupelipalvelimen osoite, johon yhdistetään käynnistyksen jälkeen"

#: mainwindow.cpp:59
#, kde-format
msgid "&Single Player"
msgstr "&Yksi pelaaja"

#: mainwindow.cpp:63
#, kde-format
msgid "&Host Game..."
msgstr "&Koneen nimi"

#: mainwindow.cpp:67
#, kde-format
msgid "&Connect to Game..."
msgstr "&Yhdistä palvelimeen…"

#: mainwindow.cpp:72
#, kde-format
msgid "Change &Nickname..."
msgstr "Vaihda &nimeä…"

#: mainwindow.cpp:75
#, kde-format
msgid "&Play Sounds"
msgstr "&Äänet"

#: mainwindow.cpp:80
#, kde-format
msgid "&Adjacent Ships"
msgstr "Vi&erekkäiset laivat"

#: mainwindow.cpp:85
#, kde-format
msgid "&Multiple Ships"
msgstr "&Useat laivat"

#: mainwindow.cpp:90
#, kde-format
msgid "Show End-of-Game Message"
msgstr "Näytä viesti pelin lopussa"

#: mainwindow.cpp:95
#, kde-format
msgid "Show &Left Grid"
msgstr "Näytä &vasen ruudukko"

#: mainwindow.cpp:99
#, kde-format
msgid "Show &Right Grid"
msgstr "Näytä &oikea ruudukko"

#: networkdialog.cpp:61
#, kde-format
msgid "&Nickname:"
msgstr "&Kutsumanimi:"

#: networkdialog.cpp:73
#, kde-format
msgid "&Join game:"
msgstr "&Liity peliin:"

#: networkdialog.cpp:88
#, kde-format
msgid "&Hostname:"
msgstr "&Koneennimi:"

#: networkdialog.cpp:101 networkdialog.cpp:131
#, kde-format
msgid "&Port:"
msgstr "&Portti:"

#: networkdialog.cpp:116
#, kde-format
msgid "&Enter server address manually"
msgstr "&Syötä palvelimen osoite itse"

#: networkdialog.cpp:148
#, kde-format
msgid "Network Parameters"
msgstr "Verkkoasetukset"

#: networkdialog.cpp:210
#, kde-format
msgid "Connecting to remote host..."
msgstr "Yhdistetään etäpalvelimeen"

#: networkdialog.cpp:217
#, kde-format
msgid "Waiting for an incoming connection..."
msgstr "Odotetaan sisään tulevaa yhteyttä…"

#: networkdialog.cpp:236
#, kde-format
msgid "Could not connect to host"
msgstr "Ei voitu yhdistää palvelimelle. "

#: networkentity.cpp:189
#, kde-format
msgid "You have "
msgstr "Sinulla on "

#: networkentity.cpp:195
#, kde-format
msgid ", "
msgstr ", "

#: networkentity.cpp:288
#, kde-format
msgid "You can place ships adjacent to each other"
msgstr "Laivoja voi sijoittaa vierekkäin"

#: networkentity.cpp:291
#, kde-format
msgid "You must leave a space between ships"
msgstr "Laivojen väliin täytyy jättää tilaa"

#: playerentity.cpp:199
#, kde-format
msgid "Your opponent is now known as %1"
msgstr "Vastustajasi nimi on nyt %1"

#: playerentity.cpp:204
#, kde-format
msgid "Your opponent disconnected from the game"
msgstr "Vastustajasi on poistunut."

#: playfield.cpp:147 playfield.cpp:167
#, kde-format
msgid "Shots"
msgstr "Laukaukset"

#: playfield.cpp:148 playfield.cpp:168
#, kde-format
msgid "Hits"
msgstr "Osumat"

#: playfield.cpp:149 playfield.cpp:169
#, kde-format
msgid "Misses"
msgstr "Ohi"

#: playfield.cpp:189
#, kde-format
msgid "You win!"
msgstr "Voitit!"

#: playfield.cpp:191
#, kde-format
msgid "You win. Excellent!"
msgstr "Voitit. Loistavaa!"

#: playfield.cpp:195
#, kde-format
msgid "You lose."
msgstr "Hävisit."

#: playfield.cpp:197
#, kde-format
msgid "You lose. Better luck next time!"
msgstr "Hävisit. Parempi onni ensi kerralla!"

#: playfield.cpp:207
#, kde-format
msgid "Change Nickname"
msgstr "Muuta nimeä"

#: playfield.cpp:207
#, kde-format
msgid "Enter new nickname:"
msgstr "Anna uusi nimi:"

#: playfield.cpp:240
#, kde-format
msgid "Restart game"
msgstr "Aloita uudelleen"

#: playfield.cpp:241
#, kde-format
msgid "Your opponent has requested to restart the game. Do you accept?"
msgstr "Vastustajasi haluaa aloitta uuden pelin. Hyväksytkö sen?"

#: playfield.cpp:242
#, kde-format
msgctxt "@action:button"
msgid "Accept"
msgstr "Hyväksy"

#: playfield.cpp:243
#, kde-format
msgctxt "@action:button"
msgid "Reject"
msgstr "Hylkää"

#: playfield.cpp:256
#, kde-format
msgid ""
"Your opponent is using a pre-KDE4 version of Naval Battle. Note that, "
"according to the rules enforced by old clients, ships cannot be placed "
"adjacent to one another and only one ship of each size is allowed."
msgstr ""
"Vastustajasi käyttää Meritaistelusta KDE 4:ää edeltävää versiota. Ota "
"huomioon, että vanhojen asiakasohjelmien sääntöjen mukaan laivoja ei voi "
"sijoittaa vierekkäin, ja samankokoisia laivoja voi olla vain yksi."

#: playfield.cpp:269
#, kde-format
msgid "Enemy has shot. Shoot now!"
msgstr "Vihollinen ampui. Ammu nyt!"

#: playfield.cpp:273
#, kde-format
msgid "Waiting for enemy to shoot..."
msgstr "Odotan vihollisen laukausta…"

#: playfield.cpp:282
#, kde-format
msgid "Ships placed. Now shoot on the enemy field!"
msgstr "Laivat on sijoitettu. Ammu kohti vihollista!"

#: playfield.cpp:285
#, kde-format
msgid "Waiting for other player to start the game..."
msgstr "Odotetaan, kunnes toinen pelaaja aloittaa pelin…"

#: playfield.cpp:289
#, kde-format
msgid "Waiting for other player to place his ships..."
msgstr "Odotetaan, kunnes toinen pelaaja on sijoittanut laivansa…"

#: playfield.cpp:303
#, kde-format
msgid "Place your %1 ships. Use the right mouse button to rotate them."
msgstr ""
"Sijoita %1 laivaasi. Voit kääntää laivan suuntaa hiiren oikealla "
"painikkeella."

#: playfield.cpp:309
#, kde-format
msgid "You can't place your remaining ships."
msgstr "Et voi sijoittaa loppuja laivojasi."

#: playfield.cpp:310
#, kde-format
msgctxt "@action"
msgid "Restart"
msgstr "Aloita alusta"

#: playfield.cpp:311
#, kde-format
msgctxt "@action"
msgid "Abort"
msgstr "Keskeytä"

#: playfield.cpp:317
#, kde-format
msgid ""
"You can't place your remaining ships. Please restart placing ships or abort "
"game"
msgstr ""
"Et voi sijoittaa loppuja laivoja. Aloita laivojen sijoittaminen alusta tai "
"keskeytä peli."

#: playfield.cpp:317
#, kde-format
msgid "Restart placing ships"
msgstr "Aloita laivojen sijoittaminen alusta"

#: simplemenu.cpp:40
#, kde-format
msgid "Single Player"
msgstr "Yksi pelaaja"

#: simplemenu.cpp:41
#, kde-format
msgid "Host Network Game"
msgstr "Isännöi verkkopeliä"

#: simplemenu.cpp:42
#, kde-format
msgid "Connect to Network Game"
msgstr "Yhdistä verkkopeliin"

#: simplemenu.cpp:132 simplemenu.cpp:149
#, kde-format
msgid "Remote player"
msgstr "Etäpelaaja"

#~ msgid "Game difficulty."
#~ msgstr "Pelin vaikeusaste."

# pmap: =/gen=KBattleshipin/
# pmap: =/elat=KBattleshipistä/
#~ msgid "KBattleship"
#~ msgstr "KBattleship"
